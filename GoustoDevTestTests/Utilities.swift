//
//  Utilities.swift
//  GoustoDevTest
//
//  Created by George Danikas on 10/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import UIKit

class Utilities {
    
    // Singleton instance
    static let instance = Utilities()
    
    /**
     Returns json object from resouce file
     */
    func jsonFromResource(resource: String) -> Data? {
        /// Get bundle
        let bundle = Bundle(for: type(of: self))
        let path = bundle.path(forResource: resource, ofType: "json")
        
        guard let filePath = path else {
            return nil
        }
        
        // Reading contents of file
        var str: String?
        do {
            str = try String(contentsOfFile: filePath)
        } catch _ {
            return nil
        }
        
        guard let jsonString = str else {
            return nil
        }
        
        guard let data = jsonString.data(using: .utf8) else {
            return nil
        }
        
        
        return data
    }

}
