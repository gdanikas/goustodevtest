//
//  ProductsServiceTests.swift
//  GoustoDevTest
//
//  Created by George Danikas on 09/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import XCTest
@testable import GoustoDevTest

class ProductsServiceTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        // Initialize CoreData stack
        CoreDataStack.sharedInstance.setDataModel("GoustoDB")
        
        // Delete all products data
        CoreDataStack.sharedInstance.deleteAllData(entity: "CDProduct")
    }
    
    override func tearDown() {
        super.tearDown()
        
        // Delete all products data
        CoreDataStack.sharedInstance.deleteAllData(entity: "CDProduct")
    }
    
    func testReadFromServer() {
        let expectation = self.expectation(description: "Check if network is reachable")
        
        ProductsService().readFromServer(completion: { (success) in
            if !success {
                XCTFail()
            }
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30.0, handler: nil)
    }
    
}


