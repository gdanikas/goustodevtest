//
//  CategoriesServiceTests.swift
//  GoustoDevTest
//
//  Created by George Danikas on 09/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import XCTest
@testable import GoustoDevTest

class CategoriesServiceTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        // Initialize CoreData stack
        CoreDataStack.sharedInstance.setDataModel("GoustoDB")
        
        // Delete all categories data
        CoreDataStack.sharedInstance.deleteAllData(entity: "CDCategory")
    }
    
    override func tearDown() {
        super.tearDown()
        
        // Delete all categories data
        CoreDataStack.sharedInstance.deleteAllData(entity: "CDCategory")
    }
    
    func testReadFromServer() {
        let expectation = self.expectation(description: "Check if network is reachable")
        
        CategoriesService().readFromServer(completion: { (success) in
            if !success {
                XCTFail()
            }
            expectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 30.0, handler: nil)
    }
    
}
