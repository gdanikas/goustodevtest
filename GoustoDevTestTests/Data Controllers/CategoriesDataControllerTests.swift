//
//  CategoriesDataControllerTests.swift
//  GoustoDevTest
//
//  Created by George Danikas on 09/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import XCTest
@testable import GoustoDevTest

class CategoriesDataControllerTests: XCTestCase {
    
    var categoriesDataController: CategoriesDataController!
    
    override func setUp() {
        super.setUp()
        
        categoriesDataController = CategoriesDataController()
        
        // Clear existing local data
        categoriesDataController.deleteLocalData()
        
        // Load data into CoreData
        self.initData()
    }
    
    override func tearDown() {
        super.tearDown()
        
        // Clear existing local data
        categoriesDataController.deleteLocalData()
    }
    
    /**
     Initialize data from json
     */
    func initData() {
        guard let data = Utilities.instance.jsonFromResource(resource: "Categories") else {
            XCTFail("The resource could not be read")
            return
        }
        
        guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] else {
            return
        }
        
        categoriesDataController.addIntoLocalData(fromJSON: json! as AnyObject, completion: {_ in })
    }
    
    func testReadFromLocalData() {
        if let _ = categoriesDataController.readFromLocalData() {
            XCTAssert(true, "Pass")
        } else {
            XCTFail()
        }
    }
    
    func testReadFromLocalDataExpectedResult() {
        
        if let results = categoriesDataController.readFromLocalData() {
            if results.count == 20 {
                XCTAssert(true, "Pass")
            } else {
                XCTFail()
            }
            
        } else {
            XCTFail()
        }
    }
    
    func testDeleteLocalData() {
        categoriesDataController.deleteLocalData()
        
        if let results = categoriesDataController.readFromLocalData() {
            if results.count == 0 {
                XCTAssert(true, "Pass")
            } else {
                XCTFail()
            }
            
        } else {
            XCTFail()
        }
    }
}
