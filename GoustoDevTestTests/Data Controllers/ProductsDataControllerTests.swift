//
//  ProductsDataControllerTests.swift
//  GoustoDevTest
//
//  Created by George Danikas on 09/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import XCTest
@testable import GoustoDevTest

class ProductsDataControllerTests: XCTestCase {
    
    var productsDataController: ProductsDataController!
    
    override func setUp() {
        super.setUp()
        
        productsDataController = ProductsDataController()
        
        // Clear existing local data
        productsDataController.deleteLocalData()
        
        // Load data into CoreData
        self.initData()
    }
    
    override func tearDown() {
        super.tearDown()
        
        // Clear existing local data
        productsDataController.deleteLocalData()
    }
    
    /**
     Initialize data from json
     */
    func initData() {
        guard let data = Utilities.instance.jsonFromResource(resource: "Products") else {
            XCTFail("The resource could not be read")
            return
        }
        
        guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] else {
            return
        }
        
        productsDataController.addIntoLocalData(fromJSON: json! as AnyObject, completion: {_ in })
    }
    
    func testReadFromLocalData() {
        if let _ = productsDataController.readFromLocalData() {
            XCTAssert(true, "Pass")
        } else {
            XCTFail()
        }
    }
    
    func testReadFromLocalDataExpectedResult() {
        
        if let results = productsDataController.readFromLocalData() {
            if results.count == 235 {
                XCTAssert(true, "Pass")
            } else {
                XCTFail()
            }
            
        } else {
            XCTFail()
        }
    }
    
    func testDeleteLocalData() {
        productsDataController.deleteLocalData()
        
        if let results = productsDataController.readFromLocalData() {
            if results.count == 0 {
                XCTAssert(true, "Pass")
            } else {
                XCTFail()
            }
            
        } else {
            XCTFail()
        }
    }
}
