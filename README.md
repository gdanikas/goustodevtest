[![N|Gousto](https://media.licdn.com/mpr/mpr/shrink_150_150/AAEAAQAAAAAAAAbIAAAAJDJhNmIwZTkzLTdiMjUtNDU3Ni1iYTRhLWIyMzNhODQ2MjI5Mw.png)](https://www.gousto.co.uk/)

## Assignment

The task will be to design and implement a standalone app that lists and displays Gousto’s additional products.
Customer adds additional products after selecting recipes and placing a box order.
Examples of additional products: wine, kitchen utensils, desserts.
 
Please have a look at two API endpoints:
 
https://api.gousto.co.uk/products/v2.0/products
Returns a list of additional products available.
 
https://api.gousto.co.uk/products/v2.0/categories
Returns a list of categories for additional products.
 
Example:
https://api.gousto.co.uk/products/v2.0/products?includes[]=categories&includes[]=attributes&image_sizes[]=750
 
* "image_sizes" array specifies that the returned object will include images with widths of requested sizes (or the closest bigger size than requested); that allows to optimize the image sizes for certain mobile devices.
* "includes" array specifies additional objects to be returned with products, for example "categories", "attributes"
 
Functional Requirements
 
* Product list should display product title, price, image (please feel free to show any additional info returned by the API).
* Product detail page with description and additional chosen info.
* There should be an option to filter products by a category. (with an "All Categories" option)
* Products and Categories should be stored in a local database. When the app is offline it should still display Products and Categories from core data (Yes the old school Core Data and not Realm)
* Products and Categories should be refreshed on the application launch and screen load to make sure there is the most recent data displayed when the app is online.

Non Functional Requirements
 
* The app should be built with good UX and use iOS native UI elements where
 possible.
* It is allowed to use any required third-party libraries. (except Realm)
* Please create some unit tests as part of the task.
* Please feel free to come up with your own design of the screens.
 
Extra Brownie Points For (not required):
 
* Create a simple automated test for the application (for example switching the
 selected category)
* Fancy transition between list and detail page

## Requirements

* Xcode 8.3+
* Swift 3.1+
* iOS 9.0+

## CocoaPods

* SwiftyBeaver
* KIF (for UI Testing)
* Nimble (for UI Testing)
 
## Features
 
* Adopting MVVM pattern
* Storyboards
* AutoLayout
* Using XIBs to layout custom views
* Using Delegation to communicate between controllers 
* Unit & UI Tests

## SOLID principles
 
Trying to apply SOLID principles and Clean Code, specially the Single Responsability. Classes must be lightewigth and perform only one task inside his abstraction layer.

## Database

I have created the following CoreData entities:

* CDCategory: List of categories for additional products
* CDProduct: List of of additional products available
* CDProductAttribute: List of of products attributes
* CDProductImage: List of of products images
 
Use one to many or one to one relationships between entities, with delete cascade or nullify rules

## Logging

Logs into the console the network requests/responses and database operations with different logs levels. It's instantiated as:
```sh
let log = SwiftyBeaver.self
```
Log level can be:
* Verbose
* Debug
* Info
* Warning
* Error
