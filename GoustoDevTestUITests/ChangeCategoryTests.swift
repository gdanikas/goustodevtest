//
//  ChangeCategoryTests.swift
//  GoustoDevTest
//
//  Created by George Danikas on 10/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import KIF
import Nimble
@testable import GoustoDevTest

class ChangeCategoryTests: KIFTestCase {
    let productsTableAccessibilityIdentifier = "Products - TableView"
    let categoriesTableAccessibilityIdentifier = "Categories - TableView"
    
    var categoriesDataController: CategoriesDataController!
    var productsDataController: ProductsDataController!

    override func beforeAll() {
        super.beforeAll()

    }
    
    override func beforeEach() {
        super.beforeEach()
        
        popToRoot()
    }
    
    func popToRoot() {
        if let navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            navigationController.popToRootViewController(animated: false)
        }
    }
    
    func testChangeCategory() {

        // Tap on categories dropdown menu
        tester().tapView(withAccessibilityLabel: "Categories - MenuView")
        
        let categoriesTableView = tester().waitForView(withAccessibilityLabel: categoriesTableAccessibilityIdentifier) as! UITableView

        // Extract category title of 4rd row in categoires table
        let indexPath = IndexPath(row: 3, section: 0)
        let categoryCell = tester().waitForCell(at: indexPath, in: categoriesTableView)
        
        var selectedCategory = ""
        if let categoryCell = categoryCell, let titleLbl = categoryCell.viewWithTag(1) as? UILabel {
            selectedCategory = titleLbl.text ?? ""
        }
        
        // Tap Tap on 4nd in categoires table
        tester().tapRow(at: indexPath, inTableViewWithAccessibilityIdentifier: categoriesTableAccessibilityIdentifier)

        // Check if the 1st row of 1st sction in products table containts the selected category
        let productsTableView = tester().waitForView(withAccessibilityLabel: productsTableAccessibilityIdentifier) as! UITableView
        let productCell = tester().waitForCell(at: IndexPath(row: 0, section: 0), in: productsTableView)

        if let productCell = productCell as? ProductViewCell {
            expect(productCell.categoryLbl.text ?? "").to(contain(selectedCategory))
        }
    }
    
}
