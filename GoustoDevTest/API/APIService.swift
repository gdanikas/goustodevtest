//
//  APIService.swift
//  GoustoDevTest
//
//  Created by George Danikas on 07/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import Foundation
import SwiftyBeaver

enum Result<T> {
    case Success(T)
    case Error(String)
}

class APIService: NSObject {
    
    // Singleton instance
    static let instance = APIService()
    
    // Log
    let log = SwiftyBeaver.self
    
    func getData(endPoint: EndPoints, _ completion: @escaping (Result<AnyObject>) -> Void) {

        guard let url = URL(string: endPoint.asUrlString()) else {
            return completion(.Error(NSLocalizedString("Invalid URL", comment: "API Service")))
        }
        
        log.verbose("REQUEST URL \(url.absoluteString)")
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard error == nil else {
                return completion(.Error(error!.localizedDescription))
            }
            
            if let httpResponse = response as? HTTPURLResponse,
                httpResponse.statusCode != 200 && httpResponse.statusCode != 204 {
                return completion(.Error(NSLocalizedString("Invalid reply", comment: "API Service")))
            }
            
            guard let data = data else {
                return completion(.Error(error?.localizedDescription ?? NSLocalizedString("No data returned", comment: "API Service")))
            }
            
            self.log.verbose("RESPONSE for URL \(url.absoluteString) \n\(response.debugDescription)")
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [.mutableContainers]) as AnyObject
                
                DispatchQueue.main.async {
                    completion(.Success(json))
                }
            } catch let error {
                return completion(.Error(error.localizedDescription))
            }
            
            }.resume()
    }
}
