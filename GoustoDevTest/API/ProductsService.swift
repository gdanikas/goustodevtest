//
//  ProductsService.swift
//  GoustoDevTest
//
//  Created by George Danikas on 06/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import Foundation
import SwiftyBeaver

class ProductsService {
    
    // Log
    let log = SwiftyBeaver.self
    
    /**
     Reads Products JSON feed from network
     
     - parameter completion:  (success: Bool)
     - returns: APISet
     */
    func readFromServer(completion: @escaping (_ success: Bool) -> Void) {
        
        APIService.instance.getData(endPoint: .products) { (result) in
            switch result {
                case .Success(let data):
                    // Add data in local storage
                    ProductsDataController().addIntoLocalData(fromJSON: data, completion: { (success) in
                        guard success else {
                            completion(false)
                            return
                        }
                        
                        completion(true)
                    })
                    
                case .Error(let message):
                    self.log.error(message)
                    
                    DispatchQueue.main.async {
                        completion(false)
                    }
                    
            }
        }
    }
}
