//
//  EndPoints.swift
//  GoustoDevTest
//
//  Created by George Danikas on 07/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import UIKit

let kBASE_URL = "https://api.gousto.co.uk/products/v2.0"

enum EndPoints: String {
    // A list of categories for additional products
    case categories = "categories"
    
    // A list of additional products available
    case products = "products?includes[]=categories&includes[]=attributes&image_sizes[]=750"
    
    func asUrlString() -> String {
       return "\(kBASE_URL)/" + self.rawValue
    }
}

