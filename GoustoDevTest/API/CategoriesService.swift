//
//  CategoriesService.swift
//  GoustoDevTest
//
//  Created by George Danikas on 06/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import Foundation
import SwiftyBeaver

class CategoriesService {
    
    // Log
    let log = SwiftyBeaver.self
    
    /**
     Reads Categories JSON feed from network
     
     - parameter completion:  (success: Bool)
     - returns: APISet
     */
    func readFromServer(completion: @escaping (_ success: Bool) -> Void) {
        
        APIService.instance.getData(endPoint: .categories) { (result) in
            switch result {
                case .Success(let data):
                    // Add data in local storage
                    CategoriesDataController().addIntoLocalData(fromJSON: data, completion: { (success) in
                        guard success else {
                            completion(false)
                            return
                        }
                        
                        completion(true)
                    })

                case .Error(let message):
                    self.log.error(message)
                    
                    DispatchQueue.main.async {
                        completion(false)
                    }
   
            }
        }
    }
    
}
