//
//  NavigationTransitionController.swift
//  GoustoDevTest
//
//  Created by George Danikas on 09/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import UIKit

class NavigationControllerDelegate: NSObject, UINavigationControllerDelegate {
    private let animator = TransitionAnimator()
    
    func navigationController(_ navigationController: UINavigationController,
                              animationControllerFor operation: UINavigationControllerOperation,
                              from fromVC: UIViewController,
                              to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return animator
    }
}


