//
//  TransitionAnimator.swift
//  GoustoDevTest
//
//  Created by George Danikas on 09/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import UIKit

class TransitionAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using context: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.4
    }
    
    func animateTransition(using context: UIViewControllerContextTransitioning) {
        guard let toVC = context.viewController(forKey: .to), let fromVC = context.viewController(forKey: .from) else {
            return
        }
        
        // Products List VC -> Product Details VC
        if let productsVC = fromVC as? ProductsListVC, let detailsVC = toVC as? ProductDetailsVC {
            moveFromProducts(productsVC: productsVC, toDetail: detailsVC, withContext: context)
        }
            
        // Product Details VC ->  Products List VC
        else if let productsVC = toVC as? ProductsListVC, let detailsVC = fromVC as? ProductDetailsVC {
            moveFromDetails(detailsVC: detailsVC, toProducts: productsVC, withContext: context)
        }
    }
    
    /**
     Controlling transition from Products List VC to Product Details VC
     */
    private func moveFromProducts(productsVC: ProductsListVC, toDetail detailsVC: ProductDetailsVC, withContext context: UIViewControllerContextTransitioning) {
        
        if let indexPath = productsVC.tableView.indexPathForSelectedRow,
            let selectedCell = productsVC.tableView.cellForRow(at: indexPath) as? ProductViewCell {
            
            context.containerView.addSubview(detailsVC.view)
            
            // Create a snapshot image from selected cell (source view)
            let snapshotFrame = selectedCell.convert(selectedCell.bounds, to: productsVC.view)
            
            let snapshotImageView = UIImageView(frame: snapshotFrame)
            snapshotImageView.contentMode = .scaleAspectFill
            snapshotImageView.clipsToBounds = true
            snapshotImageView.image = selectedCell.productImageView.image
            snapshotImageView.alpha = 0.0
            
            detailsVC.view.addSubview(snapshotImageView)
            
            // Hide source view
            productsVC.tableView.alpha = 0
            
            // Hide cells in destination view
            detailsVC.hideVisibleCells()
            
            UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1.0, options: .curveEaseOut, animations: {
                // Adjust destination table view scroll area content so it appears right under the navigation bar
                detailsVC.tableView.contentInset = UIEdgeInsetsMake(64.0, 0, 0, 0)
                
                // Move and reisze image snapshot towards the desctination product imageView
                snapshotImageView.frame = CGRect(x: 0.0, y: 0.0, width: snapshotImageView.frame.width, height: UIScreen.main.bounds.size.height * 0.4)
                snapshotImageView.alpha = 1.0
                
                // Show destination view
                detailsVC.view.alpha = 1.0
                
            }) { finished in
                
                // Remove image snapshot
                snapshotImageView.removeFromSuperview()
                
                // Show cells in destination view
                detailsVC.showVisibleCells()

                // Delesect row at source view
                productsVC.tableView.deselectRow(at: indexPath, animated: false)
                
                // Complete transition
                context.completeTransition(!context.transitionWasCancelled)
            }
        }
    }
    
    /**
     Controlling transition from Product Details VC to Products List VC
     */
    private func moveFromDetails(detailsVC: ProductDetailsVC, toProducts productsVC: ProductsListVC, withContext context: UIViewControllerContextTransitioning) {
        
        context.containerView.addSubview(productsVC.view)
        
        // Hide source view
        detailsVC.view.alpha = 0.0
        
        UIView.animate(withDuration: 0.4, animations: {
            // Show destination view
            productsVC.view.alpha = 1.0
            productsVC.tableView.alpha = 1.0
            
        }) { finished in
            // Complete transition
            context.completeTransition(!context.transitionWasCancelled)
        }
    }

    /**
     Helper method for creating image snapshot
     */
    private func createTransitionImageView(withFrame frame: CGRect) -> UIImageView {
        let imageView = UIImageView(frame: frame)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        return imageView
    }

}
