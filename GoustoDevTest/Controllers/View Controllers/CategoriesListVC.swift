//
//  CategoriesListVC.swift
//  GoustoDevTest
//
//  Created by George Danikas on 08/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import UIKit

final class CategoriesListVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    // Reuse identifier
    static let cellIdentifier = "CategoryCellID"
    
    var viewModelController: ViewModelController?
    var selectedCategoryIndex = -1
    
    var categoryDidChange: ((CategoryViewModel?, Int) -> Void)?
    var categoriesDidLoad: ((Bool) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Hide empty cells
        tableView.tableFooterView = UIView.init(frame: CGRect.zero)
        
        // Load categories
        if let viewModelController = viewModelController {
            viewModelController.loadCategories({ (success) in
                if success {
                    if let handler = categoriesDidLoad {
                        handler(true)
                    }
                    
                    self.tableView.reloadData()
                }
            })
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadData() {
        guard let viewModelController = viewModelController else {
            return
        }
        
        if selectedCategoryIndex == -1 {
            viewModelController.removeAllOption()
        } else {
            viewModelController.addAllOption()
        }
        
        tableView.reloadData()
    }
}

// MARK: - Table view data source

extension CategoriesListVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModelController = viewModelController else {
            return 0
        }
        
        return viewModelController.categoriesCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Get cell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoriesListVC.cellIdentifier) else {
            return UITableViewCell()
        }
        
        guard let viewModelController = viewModelController, let category = viewModelController.categoryViewModel(at: indexPath.row) else {
            return cell
        }
        
        if let titleLbl = cell.viewWithTag(1) as? UILabel {
            titleLbl.text = category.title
        }
        
        // Return  cell
        return cell
    }
}


// MARK: - Table view delegate

extension CategoriesListVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewModelController = viewModelController, let category = viewModelController.categoryViewModel(at: indexPath.row) else {
            if let handler = categoryDidChange {
                selectedCategoryIndex = -1
                handler(nil, -1)
            }
            
            return
        }
        
        if let handler = categoryDidChange {
            selectedCategoryIndex = indexPath.row
            
            // If "All" selected, reset category index
            if category.id.isEmpty {
                selectedCategoryIndex = -1
            }
            
            handler(category, selectedCategoryIndex)
        }
    }
}

