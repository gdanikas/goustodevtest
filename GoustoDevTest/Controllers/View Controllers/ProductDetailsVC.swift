//
//  ProductDetailsVC.swift
//  GoustoDevTest
//
//  Created by George Danikas on 09/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import UIKit

final class ProductDetailsVC: UITableViewController {

    var viewModel: ProductViewModel?
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set view title
        if let viewModel = viewModel {
            title = viewModel.title
        }
        
        // Hide empty cells
        tableView.tableFooterView = UIView.init(frame: CGRect.zero)
        
        // Self-sizing cells
        tableView.estimatedRowHeight = 44.0
        
        // Register cells
        tableView.register(UINib.init(nibName: "ProductViewCell", bundle: nil), forCellReuseIdentifier: ProductViewCell.identifier)
        tableView.register(UINib.init(nibName: "ProductDetailViewCell", bundle: nil), forCellReuseIdentifier: ProductDetailViewCell.identifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide back button title
        navigationController?.navigationBar.topItem?.title = ""
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = viewModel else {
            return 0
        }
        
        return 2 + viewModel.additionalInfo.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = viewModel else {
            return  UITableViewCell()
        }
        
        switch indexPath.row {
            // Product main info cell
            case 0:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductViewCell.identifier) as? ProductViewCell else {
                    return UITableViewCell()
                }

                // Set cell view model
                cell.viewModel = viewModel
                
                cell.isDetailed = true
                cell.isSeperatorHidden = true
                
                // Download image
                cell.productImageView.downloadImage(withURLString:  viewModel.imageUrl, placeholder: nil,  { (finished, image) in
                    // Check whether it is the right cell
                    if let updateCell = tableView.cellForRow(at: indexPath) as? ProductViewCell {
                        updateCell.productImageView.image = image
                    }
                })
                
                return cell
           
           // Product description cell
           case 1:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductDetailViewCell.identifier) as? ProductDetailViewCell else {
                    return UITableViewCell()
                }
            
                cell.title = ""
                cell.detailLbl.text = viewModel.fullDescription
                
                return cell
            
            // Product additional info cells
            default:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductDetailViewCell.identifier) as? ProductDetailViewCell else {
                    return UITableViewCell()
                }
                
                let keyIndex = indexPath.row - 2
                if viewModel.additionalInfo.keys.count > keyIndex {
                    let key = Array(viewModel.additionalInfo.keys)[keyIndex]

                    cell.title = key
                    cell.detailLbl.text = viewModel.additionalInfo[key]
                }
                
                return cell
        }
    }
    
    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
       
}

// MARK: - Helper methods for custom transitions

extension ProductDetailsVC {
    private var visibleCellViews: [UIView] {
        return (tableView.visibleCells).map { $0.contentView }
    }
    
    /**
     Hide table visible cells
     */
    func hideVisibleCells() {
        for cell in visibleCellViews {
            cell.alpha = 0.0
        }
    }
    
    /**
     Show table visible cells
     */
    func showVisibleCells() {
        showViews(visibleCellViews)
    }

    /**
     Show views one by one (recursive method)
     
     - parameter array: views to show.
     */
    func showViews(_ views: [UIView]) {
        if let view = views.first {
            view.alpha = 1.0
            UIView.animate(withDuration: 0.2, animations: {
                self.showViews(Array(views[1..<views.count]))
            })
        }
    }

}

