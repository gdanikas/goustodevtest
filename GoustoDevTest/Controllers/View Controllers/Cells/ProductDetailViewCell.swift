//
//  ProductDetailViewCell.swift
//  GoustoDevTest
//
//  Created by George Danikas on 09/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import UIKit

class ProductDetailViewCell: UITableViewCell {
    
    // Reuse identifier
    static let identifier = "DetailCellID"
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var detailLbl: UILabel!

    @IBOutlet weak var titleLblBottomConstr: NSLayoutConstraint!
    
    var title: String = "" {
        didSet {
            titleLbl.text = title
            titleLblBottomConstr.constant = title.isEmpty ? 0 : 8
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        prepareForReuse()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        title = ""
        detailLbl.text = ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
