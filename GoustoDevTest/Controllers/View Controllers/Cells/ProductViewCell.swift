//
//  ProductViewCell.swift
//  GoustoDevTest
//
//  Created by George Danikas on 09/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import UIKit

class ProductViewCell: UITableViewCell {
    // Reuse identifier
    static let identifier = "ProductCellID"
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var ribbonContView: UIView!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var seperatorView: UIView!
    
    @IBOutlet weak var productImageViewHeightConstr: NSLayoutConstraint!
    
    // Currency formatter
    lazy var currencyFormatter: NumberFormatter = {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.numberStyle = .currency
        currencyFormatter.maximumFractionDigits = 2
        currencyFormatter.usesGroupingSeparator = true

        return currencyFormatter
    }()
    
    var viewModel: ProductViewModel? {
        didSet {
            bindViewModel()
        }
    }
    
    var isSeperatorHidden: Bool = false {
        didSet {
            seperatorView.alpha = isSeperatorHidden ? 0 : 1.0
        }
    }
    
    var isDetailed: Bool = false {
        didSet {
            if isDetailed {
                productImageViewHeightConstr.constant = UIScreen.main.bounds.size.height * 0.4
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        prepareForReuse()
    }
    
    func bindViewModel() {
        guard let model = viewModel else {
            prepareForReuse()
            return
        }
        
        titleLbl.text = model.title
        priceLbl.text = currencyFormatter.string(from: NSNumber(value: model.listPrice))!
        
        if !model.categories.isEmpty {
            ribbonContView.alpha = 1.0
            categoryLbl.text = model.categories
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        ribbonContView.alpha = 0
        categoryLbl.text = ""
        titleLbl.text = ""
        priceLbl.text = ""
        
        productImageView.image = nil
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
