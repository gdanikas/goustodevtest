//
//  ProductsListVC.swift
//  GoustoDevTest
//
//  Created by George Danikas on 06/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import UIKit

final class ProductsListVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var categoriesContView: UIView!
    @IBOutlet weak var categoriesContViewBottomConstr: NSLayoutConstraint!
    
    fileprivate var menuView: DropDownMenuView!
    fileprivate var selectedCategoryIndex = -1
    fileprivate var isDataLoading = true
    
    let viewModelController = ViewModelController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Transparent navigation bar
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        // Hide empty cells
        tableView.tableFooterView = UIView.init(frame: CGRect.zero)
        
        // Self-sizing cells
        tableView.estimatedRowHeight = 120.0
        
        // Register cells
        tableView.register(UINib.init(nibName: "ProductViewCell", bundle: nil), forCellReuseIdentifier: ProductViewCell.identifier)
        
        // Hide categories container view
        categoriesContViewBottomConstr.constant = -categoriesContView.bounds.size.height
        
        // Get categories and products from server
        viewModelController.readDataFromServer {
            self.isDataLoading = false
            
            // Embed Categories List VC into container
            self.embedCategoriesListVC()
            
            // Load products from CoreData
            self.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadData(forCategory categoryId: String? = nil) {
        viewModelController.loadProducts(forCategory: categoryId) { (success) in
            if success {
                // Reload table data
                self.tableView.reloadData()
                
                if viewModelController.productsCount > 0 {
                    self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                }
            }
        }
    }
    
    /**
     Set navigation drop down menu
     */
    func createMenuView() {
        menuView = DropDownMenuView(navigationController: self.navigationController,
                                    containerView: self.navigationController!.view,
                                    title: NSLocalizedString("All categories", comment: "Goods List VC"))
        menuView.menuTitleColor = Theme.accentColor
        menuView.arrowTintColor = Theme.accentColor
        menuView.delegate = self
        
        // Set Accessibility keys for UI testing
        menuView.setValue("Categories - MenuView", forKey: "accessibilityLabel")
        menuView.setValue("Categories - MenuView", forKey: "accessibilityIdentifier")
        
        navigationItem.titleView = menuView
    }
    
    
    func embedCategoriesListVC() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "CategoriesListVC") as! CategoriesListVC
        vc.viewModelController = viewModelController
        vc.selectedCategoryIndex = selectedCategoryIndex
        
        // Categories data loaded from CoreData
        vc.categoriesDidLoad = {[weak self] (success) in
            // Set navigation drop down menu
            self?.createMenuView()
        }
        
        // Selected category changed
        vc.categoryDidChange = {[weak self] (category, selectedIndex) in
            // Hide categories list
            self?.showCategoriesList(false, completion: { (finished) in
                vc.reloadData()
            })
            self?.menuView.isShown = false
            self?.menuView.rotateArrow()
            
            self?.selectedCategoryIndex = selectedIndex
            
            // Change drop down menu title
            var menuTitle = NSLocalizedString("All categories", comment: "Goods List VC")
            if let category = category {
                menuTitle = category.title
            }
            
            self?.menuView.setMenuTitle(menuTitle)
            
            var categoryId: String? = nil
            if selectedIndex != -1, let category = category {
                categoryId = category.id
            }
            
            // Filter products for selected category
            self?.reloadData(forCategory: categoryId)
        }
        
        vc.launchInViewController(self, containerView: categoriesContView)
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showProductDetails" {
            
            if let vc = segue.destination as? ProductDetailsVC {
                var selectedProduct: ProductViewModel?
                
                // Get selected product
                if let indexPath = tableView.indexPathForSelectedRow {
                    selectedProduct = viewModelController.productViewModel(at: indexPath.section)
                    vc.viewModel = selectedProduct
                }
            }
            
        }
    }

    
    // MARK: Actions
    
    func showCategoriesList(_ showIt: Bool, completion: ((Bool) -> Void)? = nil) {
        // Show or Hide categories list
        UIView.animate(withDuration: 0.2, animations: {
            self.categoriesContViewBottomConstr.constant = showIt ? 0 : -self.categoriesContView.bounds.size.height
            self.view.layoutIfNeeded()
        }) { (finished) in
            if let completion = completion {
                completion(finished)
            }
        }
        
    }
}

// MARK: - Table view data source

extension ProductsListVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let numOfRows = viewModelController.productsCount
        
        if numOfRows == 0 && !isDataLoading {
            // Display an empty message
            let lblRect = CGRect(
                origin: CGPoint(x: 0, y: 0),
                size: view.bounds.size
            )
            
            let emptyMsgLbl = NoDataLabel.init(withFrame: lblRect, message: NSLocalizedString("No products found", comment: "Products List VC"))
            emptyMsgLbl.textColor = Theme.accentColor
            
            tableView.backgroundView = emptyMsgLbl
            tableView.separatorStyle = .none
        } else {
            tableView.backgroundView = nil
            tableView.separatorStyle = .singleLine
        }
        
        return numOfRows
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Get cell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductViewCell.identifier) as? ProductViewCell else {
            return UITableViewCell()
        }
        
        let product = viewModelController.productViewModel(at: indexPath.section)
        
        // Set cell view model
        cell.viewModel = product
        
        if let urlString = product?.imageUrl {
            // Download image
            cell.productImageView.downloadImage(withURLString: urlString, placeholder: nil,  { (finished, image) in
                // Check whether it is the right cell
                if let updateCell = tableView.cellForRow(at: indexPath) as? ProductViewCell {
                    updateCell.productImageView.image = image
                }
            })
        }
        
        // Return  cell
        return cell
    }
}


// MARK: - Table view delegate

extension ProductsListVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Show selected product details
        performSegue(withIdentifier: "showProductDetails", sender: self)
        
        // Desselect row
       // tableView.deselectRow(at: indexPath, animated: true)
    }
}


// MARK: - Drop down menu view delegate

extension ProductsListVC: DropDownMenuViewDelegate {
    func menuDidTap(isShown: Bool) {
        showCategoriesList(isShown)
    }
}

