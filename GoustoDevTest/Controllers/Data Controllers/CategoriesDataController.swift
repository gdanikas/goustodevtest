//
//  CategoriesDataController.swift
//  GoustoDevTest
//
//  Created by George Danikas on 06/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import Foundation
import CoreData

class CategoriesDataController {
    
    var fetchedResultsController = NSFetchedResultsController<NSFetchRequestResult>()
    
    // Order by title
    var sortDescriptor: NSSortDescriptor = {
        var sd = NSSortDescriptor(key: "title", ascending: true)
        return sd
    }()
    
    /**
     Read Categories from CoreData
     
     - parameter predicate: predicate to use in search.
     - parameter: completion: (success: Bool).
     */
    func readFromLocalData(_ predicate: NSPredicate? = nil) -> [CDCategory]? {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CDCategory.entityName())
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.predicate = predicate
        
        let context = CoreDataStack.sharedInstance.managedObjectContext
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                              managedObjectContext: context,
                                                              sectionNameKeyPath: nil,
                                                              cacheName: nil)
        
        do {
            try self.fetchedResultsController.performFetch()
            
            if let data = fetchedResultsController.fetchedObjects as? [CDCategory] {
                return data
            }
    
            return nil
        } catch {
            return nil
        }
    }
    
    /**
     Add Categories items into CoreData
     
     - parameter data: The data to add (Dictionary).
     - parameter: completion: (success: Bool).
     */
    func addIntoLocalData(fromJSON data: AnyObject, completion: (_ success: Bool) -> Void) {
        guard let results = data["data"] as? [[String: AnyObject]] else {
            completion(false)
            return
        }
        
        // First, delete all existining Category items from CoreData
        deleteLocalData()
        
        // Save categories JSON objects in CoreData
        do {
            try saveInCoreData(withArray: results)
            completion(true)
        } catch {
            completion(false)
        }
    }
    
    /**
     Delete all Category items from CoreData
     */
    func deleteLocalData() {
        CoreDataStack.sharedInstance.deleteAllData(entity: CDCategory.entityName())
    }

}

// MARK: - Private methods

extension CategoriesDataController {
    
    /**
     Add Category items into CoreData
     
     - parameter dictionary: The data to add (Dictionary).
     - returns: The category managed object.
     */
    fileprivate func createCategoryEntity(fromDictionary dictionary: [String: AnyObject]) -> NSManagedObject {
        let context = CoreDataStack.sharedInstance.managedObjectContext
        let item = CDCategory.init(managedObjectContext: context)
        
        item.id = dictionary["id"] as? String
        item.title = dictionary["title"] as? String
        
        // Protect against nil objects
        
        if let boxLimit = dictionary["box_limit"] as? Int32 {
            item.boxLimit = boxLimit
        } else {
            item.boxLimit = 0
        }
        
        if let isDefault = dictionary["is_default"] as? Bool {
            item.isDefault = isDefault
        } else {
            item.isDefault = false
        }
        
        if let recentlyAdded = dictionary["recently_added"] as? Bool {
            item.recentlyAdded = recentlyAdded
        } else {
            item.recentlyAdded = false
        }
        
        if let hidden = dictionary["hidden"] as? Bool {
            item.hidden = hidden
        } else {
            item.hidden = false
        }

        return item
    }
    
    /**
     Save categories JSON objects in CoreData
     
     - parameter array: The data to add (Dictionary).
     */
    fileprivate func saveInCoreData(withArray array: [[String: AnyObject]]) throws {
        _ = array.map{self.createCategoryEntity(fromDictionary: $0)}
        try CoreDataStack.sharedInstance.managedObjectContext.save()
    }
}
