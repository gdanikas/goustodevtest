//
//  ProductsDataController.swift
//  GoustoDevTest
//
//  Created by George Danikas on 06/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import Foundation
import CoreData

class ProductsDataController {
    
    var fetchedResultsController = NSFetchedResultsController<NSFetchRequestResult>()
    
    // Data formatter
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        return dateFormatter
    }()
    
    // Order by title
    var sortDescriptor: NSSortDescriptor = {
        var sd = NSSortDescriptor(key: "title", ascending: true)
        return sd
    }()
    
    // All product categories
    lazy var allCategories: [CDCategory]? = {
        return CategoriesDataController().readFromLocalData()
    }()
    
    /**
     Read Products from CoreData
     
     - parameter predicate: predicate to use in search.
     - parameter: completion: (success: Bool).
     */
    func readFromLocalData(_ predicate: NSPredicate? = nil) -> [CDProduct]? {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: CDProduct.entityName())
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.predicate = predicate
        
        let context = CoreDataStack.sharedInstance.managedObjectContext
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                              managedObjectContext: context,
                                                              sectionNameKeyPath: nil,
                                                              cacheName: nil)
        
        do {
            try self.fetchedResultsController.performFetch()
            
            if let data = fetchedResultsController.fetchedObjects as? [CDProduct] {
                return data
            }
            
            return nil
        } catch {
            return nil
        }
    }
    
    /**
     Add Products items into CoreData
     
     - parameter data: The data to add (Dictionary).
     - parameter: completion: (success: Bool).
     */
    func addIntoLocalData(fromJSON data: AnyObject, completion: (_ success: Bool) -> Void) {
        guard let results = data["data"] as? [[String: AnyObject]] else {
            completion(false)
            return
        }
        
        // First, delete all existining Product items from CoreData
        deleteLocalData()

        // Save products JSON objects in CoreData
        do {
            try self.saveInCoreData(withArray: results)
            completion(true)
        } catch {
            completion(false)
        }
    }
    
    /**
     Delete all Product items from CoreData
     */
    func deleteLocalData() {
        CoreDataStack.sharedInstance.deleteAllData(entity: CDProduct.entityName())
    }

}

// MARK: - Private methods
    
extension ProductsDataController {
    
    /**
     Add Product items into CoreData
     
     - parameter dictionary: The data to add (Dictionary).
     - returns: The product managed object.
     */
    fileprivate func createProductEntity(fromDictionary dictionary: [String: AnyObject]) -> CDProduct {
        let context = CoreDataStack.sharedInstance.managedObjectContext
        let item = CDProduct.init(managedObjectContext: context)
        
        item.id = dictionary["id"] as? String
        item.sku = dictionary["sku"] as? String
        item.title = dictionary["title"] as? String
        item.fullDescription = dictionary["description"] as? String
        
        // Protect against nil objects
        
        if let listPrice = dictionary["list_price"] as? String {
            item.listPrice = Double(listPrice)!
        } else {
            item.listPrice = 0
        }
        
        if let isVatable = dictionary["is_vatable"] as? Bool {
            item.isVatable = isVatable
        } else {
            item.isVatable = false
        }
        
        if let isForSale = dictionary["is_for_sale"] as? Bool {
            item.isForSale = isForSale
        } else {
            item.isForSale = false
        }
        
        if let ageRestricted = dictionary["age_restricted"] as? Bool {
            item.ageRestricted = ageRestricted
        } else {
            item.ageRestricted = false
        }
        
        if let boxLimit = dictionary["box_limit"] as? Int32 {
            item.boxLimit = boxLimit
        } else {
            item.boxLimit = 0
        }
        
        if let alwaysOnMenu = dictionary["always_on_menu"] as? Bool {
            item.alwaysOnMenu = alwaysOnMenu
        } else {
            item.alwaysOnMenu = false
        }
        
        if let createdAt = dictionary["created_at"] as? String, let date = dateFormatter.date(from: createdAt) as NSDate? {
            item.createdAt = date
        }
        
        // Add product categories
        if let categories = dictionary["categories"] as? [[String: AnyObject]] {
            for categoryDict in categories {
                if let categories = allCategories {
                    let categoryId = categoryDict["id"] as? String
                    if let categoryEntity = categories.filter({ $0.id == categoryId }).first {
                        item.addToCategories(categoryEntity)
                    }
                }
            }
        }
        
        // Add product attributes
        if let attributes = dictionary["attributes"] as? [[String: AnyObject]] {
            for attributeDict in attributes {
                let attributeEntity = createProductAttributeEntity(fromDictionary: attributeDict)
                item.addToAttributes(attributeEntity)
            }
        }
        
        // Add product images
        if let imagesDict = dictionary["images"] as? [String: [String: AnyObject]] {
            for imageKey in imagesDict.keys {
                if let imageDict = imagesDict[imageKey] {
                    let imageEntity = createProductImageEntity(fromDictionary: imageDict)
                    item.addToImages(imageEntity)
                }
            }
        }

        return item
    }
    
    fileprivate func createProductAttributeEntity(fromDictionary dictionary: [String: AnyObject]) -> CDProductAttribute {
        let context = CoreDataStack.sharedInstance.managedObjectContext
        let item = CDProductAttribute.init(managedObjectContext: context)
        
        item.id = dictionary["id"] as? String
        item.title = dictionary["title"] as? String
        item.unit = dictionary["unit"] as? String
        item.value = dictionary["value"] as? String

        return item
    }
    
    fileprivate func createProductImageEntity(fromDictionary dictionary: [String: AnyObject]) -> CDProductImage {
        let context = CoreDataStack.sharedInstance.managedObjectContext
        let item = CDProductImage.init(managedObjectContext: context)
        
        item.url = dictionary["url"] as? String
        
        if let width = dictionary["width"] as? Int16 {
            item.width = width
        } else {
            item.width = 0
        }
        
        return item
    }
    
    /**
     Save products JSON objects in CoreData
     
     - parameter array: The data to add (Dictionary).
     */
    fileprivate func saveInCoreData(withArray array: [[String: AnyObject]]) throws {
        _ = array.map{createProductEntity(fromDictionary: $0)}
        try CoreDataStack.sharedInstance.managedObjectContext.save()
    }
}
