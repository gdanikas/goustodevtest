//
//  ProductViewModel.swift
//  GoustoDevTest
//
//  Created by George Danikas on 08/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import UIKit

struct ProductViewModel {
    var id: String
    var title: String
    var fullDescription: String
    var listPrice: Double
    var imageUrl: String
    var categories: String
    var additionalInfo: [String: String]
}

