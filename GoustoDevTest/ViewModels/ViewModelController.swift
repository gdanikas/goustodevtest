//
//  ViewModelController.swift
//  GoustoDevTest
//
//  Created by George Danikas on 06/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import UIKit

class ViewModelController {
    
    // API services
    lazy var categoriesService = CategoriesService()
    lazy var productsService = ProductsService()
    
    // Data controllers
    fileprivate let categoriesDataController = CategoriesDataController()
    fileprivate let productsDataController = ProductsDataController()
    
    // View models
    fileprivate var categoriesList = [CategoryViewModel]()
    fileprivate var productsList = [ProductViewModel]()
    
    // Categories count
    var categoriesCount: Int {
        return categoriesList.count
    }
    
    /**
     Returns category object containing in the list for the given index
     
     - parameter index: The index of the category in the list
     - returns: The found category
     */
    func categoryViewModel(at index: Int) -> CategoryViewModel? {
        if categoriesList.count > index {
            return categoriesList[index]
        }
        
        return nil
    }
    
    // Products count
    var productsCount: Int {
        return productsList.count
    }
    
    /**
     Returns product object containing in the list for the given index
     
     - parameter index: The index of the product in the list
     - returns: The found product
     */
    func productViewModel(at index: Int) -> ProductViewModel? {
        if productsList.count > index {
            return productsList[index]
        }
        
        return nil
    }
    
    func addAllOption() {
        if let firstElement = categoriesList.first, firstElement.id != "" {
            let viewModel = CategoryViewModel(id: "", title: NSLocalizedString("All categories", comment: "Goods List VC"))
            categoriesList.insert(viewModel, at: 0)
        }
    }
    
    func removeAllOption() {
        if let firstElement = categoriesList.first, firstElement.id == "" {
            categoriesList.remove(at: 0)
        }
    }

    func readDataFromServer(_ completion: @escaping () -> Void) {
        categoriesService.readFromServer { (success) in
            guard success else {
                completion()
                return
            }
            
            self.productsService.readFromServer { (success) in
                guard success else {
                    completion()
                    return
                }
                
                completion()
            }
        }
    }
    
    func loadCategories(_ completion: (_ success: Bool) -> Void) {
        // Clear categories list array
        self.categoriesList = [CategoryViewModel]()
        
        // Fetch categories from CoreData
        if let categories = categoriesDataController.readFromLocalData() {
            for category in categories {
                let viewModel = CategoryViewModel(id: category.id ?? "", title: category.title ?? "")
                categoriesList.append(viewModel)
            }
            
            completion(true)
            return
        }
        
        completion(false)
    }
    
    func loadProducts(forCategory categoryId: String? = nil, _ completion: (_ success: Bool) -> Void) {
        // Clear products list array
        self.productsList = [ProductViewModel]()
        
        var predicate: NSPredicate? = nil
        
        // Set predicate if set
        if let categoryId = categoryId {
            predicate = NSPredicate(format: "ANY categories.id = %@", categoryId)
        }

        // Fetch products from CoreData
        if let products = productsDataController.readFromLocalData(predicate) {
            for product in products {
                var viewModel = ProductViewModel(id: product.id ?? "", title: product.title ?? "", fullDescription: product.fullDescription ?? "", listPrice: product.listPrice, imageUrl: "", categories: "", additionalInfo: [:])
                
                // Set product image
                if let images = product.images {
                    for image in images {
                        if let productImage = image as? CDProductImage, let url = productImage.url  {
                            viewModel.imageUrl = url
                            break
                        }
                    }
                }
                
                // Concatenate category titles
                if let categories = product.categories {
                    viewModel.categories = (categories.flatMap({($0 as? CDCategory)?.title}) as NSArray).componentsJoined(by: " | ")
                }
                
                // Additional info (use dictionary to distinct values)
                var additionalInfo = [String: String]()
                if let attributes = product.attributes {
                    for attribute in attributes {
                        if let productAttribute = attribute as? CDProductAttribute  {
                            if let title = productAttribute.title, let value = productAttribute.value {
                                var attributeValue = value
                                if  let unit = productAttribute.unit {
                                    attributeValue = attributeValue + " " + unit
                                }
                                
                                additionalInfo[title] = attributeValue
                            }
                        }
                    }
                    
                    viewModel.additionalInfo = additionalInfo
                }
                
                productsList.append(viewModel)
            }
            
            completion(true)
            return
        }
        
        completion(false)
    }
}

