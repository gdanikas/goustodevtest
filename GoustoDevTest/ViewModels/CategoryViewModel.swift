//
//  CategoryViewModel.swift
//  GoustoDevTest
//
//  Created by George Danikas on 08/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import UIKit

struct CategoryViewModel {
    var id: String
    var title: String
}

