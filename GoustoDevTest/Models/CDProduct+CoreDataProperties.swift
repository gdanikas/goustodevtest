//
//  CDProduct+CoreDataProperties.swift
//  GoustoDevTest
//
//  Created by George Danikas on 04/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import Foundation
import CoreData

extension CDProduct {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDProduct> {
        return NSFetchRequest<CDProduct>(entityName: "CDProduct")
    }

    @NSManaged public var ageRestricted: Bool
    @NSManaged public var alwaysOnMenu: Bool
    @NSManaged public var boxLimit: Int32
    @NSManaged public var createdAt: NSDate?
    @NSManaged public var fullDescription: String?
    @NSManaged public var id: String?
    @NSManaged public var isForSale: Bool
    @NSManaged public var isVatable: Bool
    @NSManaged public var listPrice: Double
    @NSManaged public var sku: String?
    @NSManaged public var title: String?
    @NSManaged public var categories: NSSet?
    @NSManaged public var attributes: NSSet?
    @NSManaged public var images: NSSet?
}

// MARK: Generated accessors for categories
extension CDProduct {

    @objc(addCategoriesObject:)
    @NSManaged public func addToCategories(_ value: CDCategory)

    @objc(removeCategoriesObject:)
    @NSManaged public func removeFromCategories(_ value: CDCategory)

    @objc(addCategories:)
    @NSManaged public func addToCategories(_ values: NSSet)

    @objc(removeCategories:)
    @NSManaged public func removeFromCategories(_ values: NSSet)

}

// MARK: Generated accessors for attributes
extension CDProduct {
    
    @objc(addAttributesObject:)
    @NSManaged public func addToAttributes(_ value: CDProductAttribute)
    
    @objc(removeAttributesObject:)
    @NSManaged public func removeFromAttributes(_ value: CDProductAttribute)
    
    @objc(addAttributes:)
    @NSManaged public func addToAttributes(_ values: NSSet)
    
    @objc(removeAttributes:)
    @NSManaged public func removeFromAttributes(_ values: NSSet)
    
}

// MARK: Generated accessors for images
extension CDProduct {
    
    @objc(addImagesObject:)
    @NSManaged public func addToImages(_ value: CDProductImage)
    
    @objc(removeImagesObject:)
    @NSManaged public func removeFromImages(_ value: CDProductImage)
    
    @objc(addImages:)
    @NSManaged public func addToImages(_ values: NSSet)
    
    @objc(removeImages:)
    @NSManaged public func removeFromImages(_ values: NSSet)
    
}
