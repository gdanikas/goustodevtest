//
//  CDProductAttribute+CoreDataProperties.swift
//  
//
//  Created by George Danikas on 07/05/2017.
//
//

import Foundation
import CoreData

extension CDProductAttribute {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDProductAttribute> {
        return NSFetchRequest<CDProductAttribute>(entityName: "CDProductAttribute")
    }

    @NSManaged public var id: String?
    @NSManaged public var title: String?
    @NSManaged public var unit: String?
    @NSManaged public var value: String?
    @NSManaged public var product: CDProduct?

}
