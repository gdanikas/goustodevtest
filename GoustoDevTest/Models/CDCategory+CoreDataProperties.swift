//
//  CDCategory+CoreDataProperties.swift
//  GoustoDevTest
//
//  Created by George Danikas on 04/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import Foundation
import CoreData

extension CDCategory {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDCategory> {
        return NSFetchRequest<CDCategory>(entityName: "CDCategory")
    }

    @NSManaged public var boxLimit: Int32
    @NSManaged public var hidden: Bool
    @NSManaged public var id: String?
    @NSManaged public var isDefault: Bool
    @NSManaged public var recentlyAdded: Bool
    @NSManaged public var title: String?
    @NSManaged public var products: NSSet?

}

// MARK: Generated accessors for products
extension CDCategory {

    @objc(addProductsObject:)
    @NSManaged public func addToProducts(_ value: CDProduct)

    @objc(removeProductsObject:)
    @NSManaged public func removeFromProducts(_ value: CDProduct)

    @objc(addProducts:)
    @NSManaged public func addToProducts(_ values: NSSet)

    @objc(removeProducts:)
    @NSManaged public func removeFromProducts(_ values: NSSet)

}
