//
//  CDProductImage+CoreDataClass.swift
//  
//
//  Created by George Danikas on 07/05/2017.
//
//

import Foundation
import CoreData

public class CDProductImage: NSManagedObject {
    
    // MARK: - Class methods
    
    /// entityName
    /// - returns: String
    class func entityName () -> String {
        return "CDProductImage"
    }
    
    /// entity: Entity description
    /// - parameter managedObjectContext: Managed Object Context.
    /// - returns: Entity description (NSEntityDescription).
    class func entity(_ managedObjectContext: NSManagedObjectContext!) -> NSEntityDescription! {
        return NSEntityDescription.entity(forEntityName: self.entityName(), in: managedObjectContext)
    }
    
    // MARK: - Life cycle methods
    
    /// init: Designated initializer
    /// - parameter entity: Entity description.
    /// - parameter context: Managed Object Context.
    override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext!) {
        super.init(entity: entity, insertInto: context)
    }
    
    /// init: Convenience initializer
    /// - parameter managedObjectContext: Managed Object Context.
    convenience init(managedObjectContext: NSManagedObjectContext!) {
        let entity = CDProductImage.entity(managedObjectContext)
        self.init(entity: entity!, insertInto: managedObjectContext)
    }
    
}

