//
//  CDProductImage+CoreDataProperties.swift
//  
//
//  Created by George Danikas on 07/05/2017.
//
//

import Foundation
import CoreData

extension CDProductImage {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDProductImage> {
        return NSFetchRequest<CDProductImage>(entityName: "CDProductImage")
    }

    @NSManaged public var url: String?
    @NSManaged public var width: Int16
    @NSManaged public var product: CDProduct?

}
