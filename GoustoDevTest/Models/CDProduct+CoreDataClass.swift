//
//  CDProduct+CoreDataClass.swift
//  GoustoDevTest
//
//  Created by George Danikas on 04/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import Foundation
import CoreData

public class CDProduct: NSManagedObject {

    // MARK: - Class methods
    
    /// entityName
    /// - returns: String
    class func entityName () -> String {
        return "CDProduct"
    }
    
    /// entity: Entity description
    /// - parameter managedObjectContext: Managed Object Context.
    /// - returns: Entity description (NSEntityDescription).
    class func entity(_ managedObjectContext: NSManagedObjectContext!) -> NSEntityDescription! {
        return NSEntityDescription.entity(forEntityName: self.entityName(), in: managedObjectContext)
    }
    
    // MARK: - Life cycle methods
    
    /// init: Designated initializer
    /// - parameter entity: Entity description.
    /// - parameter context: Managed Object Context.
    override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext!) {
        super.init(entity: entity, insertInto: context)
    }
    
    /// init: Convenience initializer
    /// - parameter managedObjectContext: Managed Object Context.
    convenience init(managedObjectContext: NSManagedObjectContext!) {
        let entity = CDProduct.entity(managedObjectContext)
        self.init(entity: entity!, insertInto: managedObjectContext)
    }
    
}

