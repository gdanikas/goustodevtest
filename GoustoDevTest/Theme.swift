//
//  Theme.swift
//  GoustoDevTest
//
//  Created by George Danikas on 08/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import UIKit

// Define the theme of the app
struct Theme {
    
    // Basic color
    static let accentColor = UIColor(red: 185.0/255.0, green: 57.0/255.0, blue: 34.0/255.0, alpha: 1.0)
    
}
