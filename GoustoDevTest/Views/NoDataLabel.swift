//
//  NoDataLabel.swift
//  GoustoDevTest
//
//  Created by George Danikas on 07/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import UIKit


class NoDataLabel: UILabel {
    fileprivate var emptyMessagge = NSLocalizedString("No data is currently available", comment: "")
    
    convenience init(withFrame frame: CGRect, message: String?) {
        self.init(frame: frame)
        
        if let msg = message {
            emptyMessagge = msg
        }
        
        setupView()
    }
    
    fileprivate func setupView() {
        text = emptyMessagge
        font = UIFont.systemFont(ofSize: 22.0, weight: UIFontWeightLight)
        textColor = .white
        numberOfLines = 0
        textAlignment = .center
        
        sizeToFit()
    }
}
