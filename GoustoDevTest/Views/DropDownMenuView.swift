//
//  DropDownMenuView.swift
//  GoustoDevTest
//
//  Created by George Danikas on 07/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import UIKit

class DropDownMenuViewSettings {
    var menuTitleColor: UIColor?
    var navigationBarTitleFont: UIFont!
    var arrowTintColor: UIColor?
    var arrowImage: UIImage!
    var arrowPadding: CGFloat!
    var animationDuration: TimeInterval!
    var shouldChangeTitleText: Bool!
    
    init() {
        self.defaultValues()
    }
    
    func defaultValues() {
        self.menuTitleColor = UIColor.darkGray
        self.arrowTintColor = UIColor.white
        self.navigationBarTitleFont = UIFont(name: "HelveticaNeue-Bold", size: 17.0)
        self.animationDuration = 0.3
        self.arrowImage = UIImage(named: "ic-arrow-down")
        self.arrowPadding = 15.0
        self.shouldChangeTitleText = true
    }
}

protocol DropDownMenuViewDelegate: class {
    func menuDidTap(isShown: Bool)
}

class DropDownMenuView: UIView {
    fileprivate var settings = DropDownMenuViewSettings()
    fileprivate var menuButton: UIButton!
    fileprivate var menuTitle: UILabel!
    fileprivate var menuArrow: UIImageView!
    fileprivate var backgroundView: UIView!
    
    fileprivate weak var navigationController: UINavigationController?
    
    var isShown: Bool!
    weak var delegate: DropDownMenuViewDelegate?
    
    // The color of menu title. Default is darkGrayColor()
    var menuTitleColor: UIColor! {
        get {
            return self.settings.menuTitleColor
        }
        set(value) {
            self.settings.menuTitleColor = value
        }
    }
    
    // The tint color of the arrow. Default is whiteColor()
    var arrowTintColor: UIColor! {
        get {
            return self.menuArrow.tintColor
        }
        set(color) {
            self.menuArrow.tintColor = color
        }
    }
    
    // The font of the navigation bar title. Default is HelveticaNeue-Bold, size 17.0
    var navigationBarTitleFont: UIFont! {
        get {
            return self.settings.navigationBarTitleFont
        }
        set(value) {
            self.settings.navigationBarTitleFont = value
            self.menuTitle.font = self.settings.navigationBarTitleFont
        }
    }
    
    // The animation duration of showing/hiding menu. Default is 0.3
    var animationDuration: TimeInterval! {
        get {
            return self.settings.animationDuration
        }
        set(value) {
            self.settings.animationDuration = value
        }
    }
    
    // The arrow next to navigation title
    var arrowImage: UIImage! {
        get {
            return self.settings.arrowImage
        }
        set(value) {
            self.settings.arrowImage = value.withRenderingMode(.alwaysTemplate)
            self.menuArrow.image = self.settings.arrowImage
        }
    }
    
    // The padding between navigation title and arrow
    var arrowPadding: CGFloat! {
        get {
            return self.settings.arrowPadding
        }
        set(value) {
            self.settings.arrowPadding = value
        }
    }
    
    
    // The boolean value that decides if you want to change the title text when an drop down item is selected. Default is TRUE
    var shouldChangeTitleText: Bool! {
        get {
            return self.settings.shouldChangeTitleText
        }
        set(value) {
            self.settings.shouldChangeTitleText = value
        }
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("\(#function) has not been implemented")
    }
    
    public init(navigationController: UINavigationController? = nil, containerView: UIView = UIApplication.shared.keyWindow!, title: String) {
        // Key window
        guard let window = UIApplication.shared.keyWindow else {
            super.init(frame: CGRect.zero)
            return
        }
        
        // Navigation controller
        if let navigationController = navigationController {
            self.navigationController = navigationController
        } else if let rootViewController = window.rootViewController as? UINavigationController {
            self.navigationController = rootViewController
        }
        
        // Get title size
        let titleSize = (title as NSString).size(attributes: [NSFontAttributeName: self.settings.navigationBarTitleFont])
        
        // Set frame
        let frame = CGRect(x: 0, y: 0, width: titleSize.width + (self.settings.arrowPadding + self.settings.arrowImage.size.width)*2, height: self.navigationController!.navigationBar.frame.height)
        
        super.init(frame:frame)
        
        self.isShown = false
        
        // Init button as navigation title
        self.menuButton = UIButton(frame: frame)
        self.menuButton.addTarget(self, action: #selector(self.menuButtonTapped(_:)), for: .touchUpInside)
        self.addSubview(self.menuButton)
        
        self.menuTitle = UILabel(frame: frame)
        self.menuTitle.text = title
        self.menuTitle.textColor = self.menuTitleColor
        self.menuTitle.font = self.settings.navigationBarTitleFont
        self.menuButton.addSubview(self.menuTitle)
        
        self.menuArrow = UIImageView(image: self.settings.arrowImage.withRenderingMode(.alwaysTemplate))
        self.menuButton.addSubview(self.menuArrow)
        
        // Init properties
        self.setupDefaultSettings()
    }
    
    override open func layoutSubviews() {
        self.menuTitle.sizeToFit()
        self.menuTitle.center = CGPoint(x: self.frame.size.width/2, y: self.frame.size.height/2)
        self.menuTitle.textColor = self.settings.menuTitleColor
        self.menuArrow.sizeToFit()
        self.menuArrow.center = CGPoint(x: self.menuTitle.frame.maxX + self.settings.arrowPadding, y: self.frame.size.height/2)
    }
    
    
    func setupDefaultSettings() {
        self.menuTitleColor = self.navigationController?.navigationBar.titleTextAttributes?[NSForegroundColorAttributeName] as? UIColor
        self.arrowTintColor = self.settings.arrowTintColor
    }

    func showMenu() {
        self.isShown = !self.isShown
        
        // Rotate arrow
        self.rotateArrow()
        
        // Call delegate
        delegate?.menuDidTap(isShown: isShown)
    }
    
    func rotateArrow() {
        UIView.animate(withDuration: self.settings.animationDuration, animations: {[weak self] () -> () in
            if let selfie = self {
                let angle: CGFloat = selfie.isShown == true ? (180.0 * CGFloat.pi) / 180.0 : (180.0 * CGFloat.pi) * 180.0
                selfie.menuArrow.transform = CGAffineTransform(rotationAngle: angle)
            }
        })
    }
    
    func setMenuTitle(_ title: String) {
        self.menuTitle.text = title
        setNeedsLayout()
    }
    
    func menuButtonTapped(_ sender: UIButton) {
        showMenu()
    }
}
