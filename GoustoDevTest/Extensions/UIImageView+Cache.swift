//
//  UIImageView+Cache.swift
//  GoustoDevTest
//
//  Created by George Danikas on 08/05/2017.
//  Copyright © 2017 George Danikas. All rights reserved.
//

import UIKit

let imageCache = NSCache<NSString, UIImage>()

extension UIImageView {
    
    func downloadImage(withURLString URLString: String, placeholder: UIImage?, _ completion: @escaping (_ finished: Bool, _ image: UIImage?) -> Void) {
        // Check if image is cahced
        if let image = imageCache.object(forKey: NSString(string: URLString)) {
            DispatchQueue.main.async {
                completion(true, image)
            }
            return
        }

        // Display a placeholder while downloading the image
        DispatchQueue.main.async {
            completion(false, placeholder)
        }
        
        if let url = URL(string: URLString) {
            // Download the image
            URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                DispatchQueue.main.async {
                    if error != nil {
                        completion(true, nil)
                        return
                    }
                    
                    if let data = data {
                        if let image = UIImage(data: data) {
                            // Cache image
                            imageCache.setObject(image, forKey: NSString(string: URLString))
                            
                            completion(true, image)
                        }
                    } else {
                        completion(true, nil)
                    }
                }
            }).resume()
        } else {
            completion(true, placeholder)
        }
    }
}
