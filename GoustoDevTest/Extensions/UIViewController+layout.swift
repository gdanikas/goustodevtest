//
//  UIViewController+layout.swift
//  ModularFoundation
//
//  Created by Brice Pollock on 6/13/15.
//  Copyright (c) 2015 Brice Pollock. All rights reserved.
//

import Foundation
import UIKit

public extension UIViewController {
    public func launchInViewController(_ containerViewController: UIViewController, containerView: UIView?) {
        // Remove first
        willMove(toParentViewController: nil)
        view.removeFromSuperview()
        removeFromParentViewController()
        
        containerViewController.addChildViewController(self)
        
        if let containerView = containerView {
            containerView.addAndFillWithSubview(view)
        } else {
            containerViewController.view.addAndFillWithSubview(view)
        }
        
        didMove(toParentViewController: containerViewController)
    }
}
